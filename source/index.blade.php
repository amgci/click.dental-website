@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Your partner for seamless digital dentistry',
    'meta_description' => 'Click.Dental is a fully digital dental laboratory that provides a seamless, online experience for all clinicians who have invested in digital dentistry.'
    ])
@endsection

@section('masthead')
<section class="masthead welcome text-center" data-scroll-index="1" style="height: calc(100vh + 6rem)">
    <div class="overlay-bg-80 flex-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-4 text-right">
                    <h1>Embrace Digital Innovation with Click.Dental</h1>
                    <div data-aos="fade-in">
                        <button class="btn btn-lg btn-primary" data-toggle="modal" data-target="#get-started">Get Started</button>
                        <button class="btn btn-lg btn-primary" data-toggle="modal" data-target="#file-uploader">File Uploader</button>
                        <button class="btn btn-lg btn-primary" data-scroll-nav="6">FAQs</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('body')
<section class="bg-skew accent-bottom">
    <div class="container" data-scroll-index="2">
        <div class="row align-items-center">
            <div class="d-none d-lg-block col-lg-5" data-aos="fade-right">
                <picture class="shadow" style="width: 100%;">
                    <source media="(max-width: 991px)" srcset="/images/digital-dentistry-1x.jpg">
                    <source media="(min-width: 1200px)" srcset="/images/digital-dentistry-2x.jpg">
                    <img src="/images/digital-dentistry-1x.jpg" alt="image">
                </picture>
            </div>
            <div class="text-center col-lg-7 text-lg-right">
                <h2>Your Digital Dentistry Partner</h2>
                <p>Click.Dental is a fully digital dental laboratory that provides a seamless, online experience for all clinicians who have invested in digital dentistry. All our high-quality products are fabricated with the latest technology to ensure high precision and quality for every case. We are proud to offer our restorations at an economic price that is possible thanks to the efficiency provided by a fully digital workflow. Click.Dental serves clinicians throughout the United States and invites any digital doctor to send us a case and discover the convenience and high-quality of a fully digital lab.</p>
                <button class="btn btn-lg btn-primary" data-toggle="modal" data-target="#get-started">Send Your Case</button>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container" data-scroll-index="3">
        <div class="row align-items-center">
            <div class="col-lg-7">
                <h2>Our Fully Digital Workflow</h2>
                <p>There are numerous benefits that come from embracing a fully digital workflow. Click.Dental wants to give these advantages to all our clinicians. Our lab is an authorized Sirona Connect Laboratory and is equipped with state-of-the-art digital dentistry processes and equipment. The benefits that come from our dedication to digital advancement includes the following:</p>
                <ul class="feature-list">
                    <li>We accept digital scans from all major intraoral scanners.</li>
                    <li>You can easily consult with our technicians in our lab while your patient is in your chair.</li>
                    <li>Capable of fabricating digital dentures.</li>
                    <li>Near perfect precision, fit, function, and esthetics.</li>
                    <li>No hassle with elimination of traditional impressions.</li>
                    <li>Improved patient satisfaction.</li>
                    <li>High economic value from digital efficiency.</li>
                    <li>One-day turnaround times for single-unit monolithic restorations.</li>
                </ul>
                <button class="btn btn-lg btn-primary" data-toggle="modal" data-target="#get-started">Get Started</button>
            </div>
            <div class="d-none d-lg-block col-lg-5" data-aos="fade-left">
                <picture class="shadow" style="width: 100%;">
                    <source media="(max-width: 991px)" srcset="/images/digital-workflow-1x.jpg">
                    <source media="(min-width: 1200px)" srcset="/images/digital-workflow-2x.jpg">
                    <img src="/images/digital-workflow-1x.jpg" alt="image">
                </picture>
            </div>
        </div>
    </div>
</section>
<section class="bg-dark cta-window p-0" data-scroll-index="4">
    <div class="overlay-bg-80" style="padding: 5rem 0 4rem;">
        <div class="container text-center">
            <h2>Ready to Go Digital?</h2>
            <p>Are you interested in integrating digital dentistry into your practice? Learn more by contacting our team today.</p>
            <button class="btn btn-lg btn-primary" data-scroll-nav="7">Contact Us</button>
        </div>
    </div>
</section>
<section id="products" class="products">
    <div class="container text-center" data-scroll-index="5">
        <h2>Our Products</h2>
        <div class="row">
            <div class="col-lg-4" data-aos="fade-down-right">
                <div class="inner-col">
                    <img src="/images/fcz-sm.png" alt="full-contour zirconia crown">
                    <h3 class="h5">Full-Contour Zirconia</h3>
                    High Flexural Strength<br>
                    Anterior and Posterior
                    <div class="more-info" data-toggle="modal" data-target="#product-fcz">
                        <span class="btn btn-lg btn-primary" data-toggle="modal" data-target="#product-fcz">View Details</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4" data-aos="fade-down">
                <div class="inner-col">
                    <img src="/images/emax-sm.png" alt="IPS e.max anterior crown">
                    <h3 class="h5">IPS e.max<sup>&reg;</sup></h3>
                    Up to 500 MPa<br>
                    Four Levels of Translucency
                    <div class="more-info" data-toggle="modal" data-target="#product-emax">
                        <span class="btn btn-lg btn-primary" data-toggle="modal" data-target="#product-emax">View Details</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4" data-aos="fade-down-left">
                <div class="inner-col">
                    <img src="/images/pfz-sm.png" alt="porcelain-fused-to-zirconia anterior crown">
                    <h3 class="h5">Porcelain-Fused-to-Zirconia</h3>
                    Lifelike Coloration<br>
                    Hypoallergenic and Biocompatible
                    <div class="more-info" data-toggle="modal" data-target="#product-pfz">
                        <span class="btn btn-lg btn-primary" data-toggle="modal" data-target="#product-pfz">View Details</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4" data-aos="fade-right">
                <div class="inner-col">
                    <img src="/images/pekkton-sm.png" alt="Pekkton® Ivory partial removable denture">
                    <h3 class="h5">Pekkton<sup>®</sup> Ivory</h3>
                    High Flexibility<br>
                    Increased Accuracy
                    <div class="more-info" data-toggle="modal" data-target="#product-pekkton">
                        <span class="btn btn-lg btn-primary" data-toggle="modal" data-target="#product-pekkton">View Details</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4" data-aos="fade-in">
                <div class="inner-col">
                    <img src="/images/sr-hybrid-sm.png" alt="screw-retained zirconia hybrid">
                    <h3 class="h5">Screw-Retained Hybrid</h3>
                    Ideal Predictability<br>
                    High Esthetics
                    <div class="more-info" data-toggle="modal" data-target="#product-sr-hybrid">
                        <span class="btn btn-lg btn-primary" data-toggle="modal" data-target="#product-sr-hybrid">View Details</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4" data-aos="fade-left">
                <div class="inner-col">
                    <img src="/images/abutment-sm.png" alt="custom titanium abutment">
                    <h3 class="h5">Custom Abutments</h3>
                    High-Quality Titanium or Zirconia<br>
                    Lifelike Emergence and Shape
                    <div class="more-info" data-toggle="modal" data-target="#product-abutment">
                        <span class="btn btn-lg btn-primary" data-toggle="modal" data-target="#product-abutment">View Details</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4" data-aos="fade-up-right">
                <div class="inner-col">
                    <img src="/images/surgical-guides-sm.png" alt="surgical guide">
                    <h3 class="h5">Surgical Guides</h3>
                    Safe, Predictable, and Efficient Surgeries<br>
                    Applicable for Broad Range of Cases
                    <div class="more-info" data-toggle="modal" data-target="#product-surgical-guides">
                        <span class="btn btn-lg btn-primary" data-toggle="modal" data-target="#product-surgical-guides">View Details</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4" data-aos="fade-up">
                <div class="inner-col">
                    <img src="/images/aligners-sm.png" alt="clear sequential aligners">
                    <h3 class="h5">Clear Sequential Aligners</h3>
                    Invisible, Durable, and Comfortable<br>
                    Suitable for Mild to Advanced Treatment
                    <div class="more-info" data-toggle="modal" data-target="#product-aligners">
                        <span class="btn btn-lg btn-primary" data-toggle="modal" data-target="#product-aligners">View Details</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4" data-aos="fade-up-left">
                <div class="inner-col product-coming-soon">
                    <img src="/images/denture-sm.png" alt="digitally fabricated full denture">
                    <h3 class="h5">Digital Dentures</h3>
                    Comfortable Fit and Function<br>
                    Digitally Fabricated
                    <div class="more-info" data-toggle="modal" data-target="#product-denture">
                        <span class="btn btn-lg btn-primary" data-toggle="modal" data-target="#product-denture">View Details</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-skew accent-top section-faq">
    <div class="container" data-scroll-index="6">
        <div class="row align-items-center">
            <div class="col-lg-7" id="faq-col">
                <h2>Frequently Asked Questions</h2>
                <div class="collapse-accordion">
                    <h3 id="faq-1" class="h6" data-toggle="collapse" data-target="#faqa-1" aria-expanded="true" aria-controls="faqa-1">Do you accept digital impressions from all intraoral scanners?</h3>
                    <div id="faqa-1" class="collapse show" aria-labelledby="faq-1" data-parent="#faq-col" style="font-size: 16px;">
                        <p>Yes, Click.Dental accepts files from all major intraoral scanners, including the following: CEREC®, iTero®, TRIOS®, 3M™ True Definition, Planmeca® Emerald™, and Carestream.<br>
                        You can submit exported .STL files through our file uploader. Please contact our team at 443.254.0050 for assistance in submitting files directly from your IOS.</p>
                    </div>
                    <h3 id="faq-2" class="h6" data-toggle="collapse" data-target="#faqa-2" aria-expanded="false" aria-controls="faqa-2">Does Click.Dental accept traditional and digital cases?</h3>
                    <div id="faqa-2" class="collapse" aria-labelledby="faq-2" data-parent="#faq-col" style="font-size: 16px;">
                        <p>Click.Dental is a fully digital dental laboratory. We only accept digital impressions. If you are looking for a laboratory that accepts traditional impressions, please visit the websites of our dental laboratory partners.</p>
                        <p>
                            <a href="http://www.costpluslab.com/" target="_blank" class="mr-3 mr-lg-5"><img src="/images/logo-cost-plus.png" alt="Cost Plus Dental Lab"></a>
                            <a href="http://triplecrownlab.com/" target="_blank"><img src="/images/logo-triple-crown.png" alt="Triple Crown Dental Lab"></a>
                        </p>
                        
                    </div>
                    <h3 id="faq-3" class="h6" data-toggle="collapse" data-target="#faqa-3" aria-expanded="false" aria-controls="faqa-3">Do you fabricate metal-based or removable restorations?</h3>
                    <div id="faqa-3" class="collapse" aria-labelledby="faq-3" data-parent="#faq-col" style="font-size: 16px;">
                        <p>Click.Dental only provides all-ceramic restorations, so we do not offer PFMs or full cast products. We currently do not offer any removable products, however as digital dentistry continues to advance, our team expects to be able to fabricate digital dentures in the near future. Our product line-up will be updated to reflect this addition when we being offering digital dentures.</p>
                    </div>
                    <h3 id="faq-4" class="h6" data-toggle="collapse" data-target="#faqa-4" aria-expanded="false" aria-controls="faqa-4">What are your turnaround times?</h3>
                    <div id="faqa-4" class="collapse" aria-labelledby="faq-4" data-parent="#faq-col" style="font-size: 16px;">
                        <p>Below are Click.Dental's in-lab turnaround times. Digital impressions must be received by our laboratory by 6:00 pm PST to qualify for 1-day turnaround.</p>
                        Modeless Single-Unit Crowns - 1-Day<br>
                        Multi-Units - 5 or More Days<br>
                        Custom Abutments - 5 or More Days<br>
                        Digital Denture - 5 or More Days<br>
                        Surgical Guides - 5 or More Days<br>
                        Clear Sequential Aligners - 5 or More Days
                    </div>
                </div>
            </div>
            <div class="d-none d-lg-block col-lg-5" data-aos="fade-left">
                <picture class="shadow" style="width: 100%;">
                    <source media="(max-width: 991px)" srcset="/images/faq-1x.jpg">
                    <source media="(min-width: 1200px)" srcset="/images/faq-2x.jpg">
                    <img src="/images/faq-1x.jpg" alt="image">
                </picture>
            </div>
        </div>
    </div>
</section>
@endsection

@section('post-footer')
    @component('_components.product-modal')
        @slot('tag')
            fcz
        @endslot
        @slot('title')
            Full-Contour Zirconia
        @endslot
        The full-contour zirconia restorations from Click.Dental are expertly fabricated using digital dentistry. We utilize the most advanced CAD/CAM equipment in the industry to ensure high esthetics, smooth surfaces, and precise fit for every full-contour zirconia restoration. Our digital dentistry expertise allows us to provide durable full-contour zirconia that will not be abrasive or damaging on opposing dentition. Full-contour zirconia is a metal-free, 100% biocompatible solution that offers the highest flexural strength of any all-ceramic restoration.
    @endcomponent
    
    @component('_components.product-modal')
        @slot('tag')
            emax
        @endslot
        @slot('title')
            IPS e.max<sup>&reg;</sup>
        @endslot
        IPS e.max<sup>&reg;</sup> is an ideally esthetic all-ceramic solution. Click.Dental's IPS e.max<sup>&reg;</sup> restorations offer lifelike esthetics for every patient due to its four levels of translucency. IPS e.max<sup>&reg;</sup> is fabricated out of the innovative lithium disilicate glass-ceramic. It is fracture-resistant, offers 2.5 to 3 times more strength than alternative materials, and has a flexural strength of up to 500 MPa. It also has additional impulse ingots, which allow for maximum flexibility. IPS e.max<sup>&reg;</sup> is highly customizable and can be fabricated as a full-contour monolithic or cut-back and layered with porcelain for anterior esthetics.
    @endcomponent
    
    @component('_components.product-modal')
        @slot('tag')
            pfz
        @endslot
        @slot('title')
            Porcelain-Fused-to-Zirconia
        @endslot
        Porcelain-fused-to-zirconia restorations from Click.Dental provide comparable strength to porcelain-fused-to-metal solutions without the added esthetic issues that can arise from a metal substructure. Fabricated out of a high-quality zirconia substructure, PFZs offer a high amount of strength and translucency that does not interfere with the esthetic porcelain overlay. Porcelain-fused-to-zirconia restorations are hypoallergenic and biocompatible. They also provide a warmer and more lifelike coloration.
    @endcomponent
       
    @component('_components.product-modal')
        @slot('tag')
            pekkton
        @endslot
        @slot('title')
            Pekkton<sup>®</sup> Ivory
        @endslot
        Click.Dental's Pekkton<sup>®</sup> Ivory restorations offer lifelike restorations and shock absorption for increased strength. This material is lightweight and ensures consistent quality. Pekkton<sup>®</sup> Ivory also has similar characteristics to biological materials, such as bone and teeth. 
    @endcomponent
         
    @component('_components.product-modal')
        @slot('tag')
            sr-hybrid
        @endslot
        @slot('title')
            Screw-Retained Hybrid
        @endslot
        The screw-retained hybrid from Click.Dental is an excellent alternative to removable prostheses. Your edentulous patients will greatly appreciate this replacement due to its increased esthetics and lifelike function. 
    @endcomponent
    
    @component('_components.product-modal')
        @slot('tag')
            abutment
        @endslot
        @slot('title')
            Custom Abutments
        @endslot
        We fabricate highly precise custom abutments, so you can be sure that every implant surgery will have ideal results. We utilize high-quality titanium and zirconia materials to craft our custom abutments. These restorations will imitate the natural emergence and shape of your patient's previous dentition. They are compatible with most major implant manufacturers and provide ideal support for the surrounding gum tissue.
    @endcomponent
    
    @component('_components.product-modal')
        @slot('tag')
            surgical-guides
        @endslot
        @slot('title')
            Surgical Guides
        @endslot
        Click.Dental's surgical guides ensure safe, predictable, and efficient implant surgeries. Applicable for a broad range of implant cases, these guides will help you navigate from beginning to end of the surgery. Every step will be carefully planned, so the results meet your practice's and your patient's high standards for a long-lasting and esthetic restorative solution.
    @endcomponent
    
    @component('_components.product-modal')
        @slot('tag')
            aligners
        @endslot
        @slot('title')
            Clear Sequential Aligners
        @endslot
        Smile Shapers Aligners powered by Click.Dental provide an efficient and esthetic alignment process for patients who require mild to advanced treatment. They are fabricated out of a biocompatible resin that is practically invisible, durable, and comfortable. These aligners seamlessly blend in with patients' teeth and life. To send your Smile Shapers case to Click.Dental, please send your digital impression and a filled out pre-submission Rx form.
    @endcomponent
    
    @component('_components.product-modal')
        @slot('tag')
            denture
        @endslot
        @slot('title')
            Digital Dentures
        @endslot
        Digital dentures from Click.Dental provide comfortable fit and function. Through the use of technology, we are able to digitally fabricate highly esthetic, comfortable, and functional prostheses that will ensure patient satisfaction.
    @endcomponent

<div id="get-started" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5>Getting Started with Click.Dental</h5>
                <p>Click.Dental accepts files from all major intraoral scanners. Sending us your case is easy and fast. Simply find your preferred scanner in the list below and use our provided protocols to connect with our laboratory. For clear aligner cases, please fill out a pre-submission Rx form and include it with your digital impression.</p>
                <a href="" class="btn btn-primary">Pre-Submission Rx Form</a>
                <hr>
                <h5>File Uploader</h5>
                <p>Please use our convenient drag-and-drop file uploader to provide us with your exported .STL file and any other files necessary for your case. Call our team at 443.254.0050 with any questions or if you need technical support.</p>
                <button class="btn btn-primary" data-target="#file-uploader">File Uploader</button>
                @if (1 == 0)
                <hr>
                <h5>Send Your Digital Files</h5>
                <h3 id="digital-itero" class="h6" data-toggle="collapse" data-target="#digital-itero-info" aria-expanded="true" aria-controls="digital-itero-info">iTero<sup>®</sup></h3>
                <div id="digital-itero-info" class="collapse show" aria-labelledby="digital-itero" data-parent="#get-started" style="font-size: 16px;">
                    <p>
                        Call 800-577-8767 and select option 1.<br>
                        Request that Click.Dental is added to your scanner, and identify our lab using our phone number: <em style="color: red">877-817-7770 (correct?)</em>.<br>
                        After Click.Dental has been added, restart your scanner.<br>
                        After connecting to us as a lab, select Click.Dental on your scanner when sending files.
                    </p>
                </div>
                <h3 id="digital-trios" class="h6" data-toggle="collapse" data-target="#digital-trios-info" aria-expanded="false" aria-controls="digital-trios-info">3Shape TRIOS<sup>®</sup></h3>
                <div id="digital-trios-info" class="collapse" aria-labelledby="digital-trios" data-parent="#get-started" style="font-size: 16px;">
                    <p>
                        Go to us.3shapecommunicate.com in a web browser.<br>
                        Connect with Click.Dental as lab by searching digital@click.dental.<br>
                        After connecting to us as a lab, select Click.Dental when sending files.
                    </p>
                </div>
                <h3 id="digital-3m" class="h6" data-toggle="collapse" data-target="#digital-3m-info" aria-expanded="false" aria-controls="digital-3m-info">3M™ True Definition Scanner</h3>
                <div id="digital-3m-info" class="collapse" aria-labelledby="digital-3m" data-parent="#get-started" style="font-size: 16px;">
                    <p>
                        Call 3M™ support at 800-634-2249, and select option 3.<br>
                        Select option 1 and request Click.Dental be added to your scanner.<br>
                        3M™ will then confirm with Click.Dental and add connection remotely.
                    </p>
                </div>
                <h3 id="digital-planmeca" class="h6" data-toggle="collapse" data-target="#digital-planmeca-info" aria-expanded="false" aria-controls="digital-planmeca-info">Planmeca<sup>®</sup> Emerald™</h3>
                <div id="digital-planmeca-info" class="collapse" aria-labelledby="digital-planmeca" data-parent="#get-started" style="font-size: 16px;">
                    <p>
                        Select Find a Lab option on your scanner.<br>
                        Search for Click.Dental or digital@click.dental.<br>
                        Add Click.Dental.<br>
                        Select Click.Dental when submitting scans.
                    </p>
                </div>
                <h3 id="digital-carestream" class="h6" data-toggle="collapse" data-target="#digital-carestream-info" aria-expanded="false" aria-controls="digital-carestream-info">Carestream<sup>®</sup></h3>
                <div id="digital-carestream-info" class="collapse" aria-labelledby="digital-carestream" data-parent="#get-started" style="font-size: 16px;">
                    <p>
                        Visit Carestream Connect on your scanner.<br>
                        Search for Click.Dental.<br>
                        Add Click.Dental.<br>
                        Select Click.Dental when submitting files.<br>
                        Email Click.Dental at digital@click.dental.
                    </p>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div id="file-uploader" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5>File Uploader</h5>
                <p>Please use our convenient drag-and-drop file uploader to provide us with your exported .STL file and any other files necessary for your case.<br>
                Call our team at 443.254.0050 with any questions or if you need technical support.</p>
                <div id="file-upload"><p style="color: red"><small>The file uploader application could not load.<br />Please make sure you're using the latest version of a supported web browser (Chrome, Firefox, Edge, Safari), with javascript enabled.</small></p></div><script type="text/javascript">var loader=function(){var e=document.createElement("script"),t=document.getElementsByTagName("script")[0];e.src="https://goron.amgservers.com/embed/62DC6522/uploader.js?v=1.0.4",t.parentNode.insertBefore(e,t)};window.addEventListener?window.addEventListener("load",loader,!1):window.attachEvent("onload",loader);</script>
            </div>
        </div>
    </div>
</div>
@endsection
