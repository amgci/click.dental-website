<div id="product-{{ $tag }}" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <img src="/images/{{ $tag }}.png" alt="{{ strip_tags($title) }}">
                </div>
                <h5>{{ $title }}</h5>
                <p>{{ $slot }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-target="#get-started">Send a Case</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>