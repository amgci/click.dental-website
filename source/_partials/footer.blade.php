<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <h2>Contact Us</h2>
                <p>
                    Our team is your resource for all things digital.
                    <br>Please contact us today if you have any questions or comments.
                </p>
                <div class="row">
                    <div class="col-lg-6">
                        <ul class="list-unstyled">
                            <li>Monday 8am - 5pm</li>
                            <li>Tuesday 8am - 5pm</li>
                            <li>Wednesday 8am - 5pm</li>
                            <li>Thursday 8am - 5pm</li>
                            <li>Friday 8am - 5pm</li>
                        </ul>
                    </div>
                    <div class="col-lg-6 text-yellow">
                        443.254.0050<br>
                        info@click.dental<br>
                        <span class="icon"><i class="fab fa-facebook-f"></i></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 align-self-end">
                <form>
                    <div class="form-row mb-2">
                        <div class="col-6">
                            <input type="text" class="form-control" placeholder="Your Name">
                        </div>
                        <div class="col-6">
                            <input type="email" class="form-control" placeholder="Email Address">
                        </div>
                    </div>
                    <textarea class="form-control mb-2" id="exampleFormControlTextarea1" placeholder="What can we do for you?"></textarea>
                    <button class="btn btn-primary" type="submit">Send Message</button>
                </form>
            </div>
        </div>
    </div>
</section>
<section class="copyright">
    <div class="container">
        <div class="row align-items-center">
            <div class="text-center col-lg-6 text-lg-left">
                <span>&copy; {{ date('Y') }} {{ $page->site_name }} - <em>All Rights Reserved</em></span>
            </div>
            <div class="text-center col-lg-6 text-lg-right">
                <a href="https://amgci.com?cref=1"><img src="/images/amg.png" alt="Dental Laboratory Marketing by AMG Creative"></a>
            </div>
        </div>
    </div>
</section>