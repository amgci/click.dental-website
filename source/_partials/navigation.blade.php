<nav class="navbar navbar-expand-lg" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="true" aria-label="Toggle navigation"><span class="fa fa-bars"></span></button>
            <a class="navbar-brand nav-link" href="#"><img src="/images/logo.png" alt="" id="logo" class="logo"></a>
        </div>
        <div id="main-navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-left">
                <li class="nav-item">
                    <button class="btn btn-link nav-link" data-scroll-nav="2">Why Digital</button>
                </li>
                <li class="nav-item">
                    <button class="nav-link btn btn-link" data-scroll-nav="3">Our Workflow</button>
                </li>
                <li class="nav-item">
                    <button class="nav-link btn btn-link" data-scroll-nav="5">Products</button>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="nav-item">
                    <button class="nav-link btn btn-link" data-scroll-nav="6">FAQs</button>
                </li>
                <li class="nav-item">
                    <button class="nav-link btn btn-link" data-scroll-nav="7">Contact</button>
                </li>
                <li class="nav-item">
                    <button class="nav-link btn btn-primary" data-toggle="modal" data-target="#get-started">Get Started</button>
                </li>
                <li class="nav-item">
                    <button class="nav-link btn btn-primary" data-toggle="modal" data-target="#file-uploader">File Uploader</button>
                </li>
            </ul>
        </div>
    </div>
</nav>