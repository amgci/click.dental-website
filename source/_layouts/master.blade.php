<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        @yield('meta')
        
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700|Montserrat:800" rel="stylesheet">
        <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
        <link rel="icon" type="image/png" href="/images/favicon.png">
        <link rel="stylesheet" href="{{ mix('css/main.css', 'assets/build') }}">
    </head>
    <body>
        <header>
            @include('_partials.navigation')
            @if (1 == 0)
            <div class="staging-notice">
                <div class="container">
                    <span><strong>Please Note:</strong> This website is currently under active development and has not been thoroughly tested.</span>
                </div>
            </div>
            @endif
        </header>
        
        @yield('masthead')
        
        <main>
            @yield('body')
        </main>
        
        <div data-scroll-index="7"></div>
        <footer class="bg-dark fixed-bottom" style="z-index: -2">
            @include('_partials.footer')
        </footer>
        
        @yield('post-footer')
        
        <script src="{{ mix('js/main.js', 'assets/build') }}"></script>
        
        <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
        <script>
                AOS.init();
        </script>
    </body>
</html>
