import $ from 'jquery';
import 'popper.js';
import 'bootstrap';
import 'scrollit';
//import Sammy from 'sammy';

$(function(){
  $.scrollIt({
      scrollTime: 400,
      topOffset: -128
  });
});

$(document).ready(() => {
    var pendingModal = null;
    $('.modal-content button[data-target="#file-uploader"]').click((event) => {
        const modal = $(event.currentTarget).parents('.modal');
        modal.on('hidden.bs.modal', (event) => {
            $('#file-uploader').modal('show');
            modal.unbind(event);
        });
        modal.modal('hide');
    });
    $('.modal-content button[data-target="#get-started"]').click((event) => {
        pendingModal = '#get-started';
        const modal = $(event.currentTarget).parents('.modal');
        modal.on('hidden.bs.modal', (event) => {
            $('#get-started').modal('show');
            modal.unbind(event);
        });
        modal.modal('hide');
    });
});

//var app = Sammy(function() {
//    this.get('/', function() {
//        console.log('hi');
//    });
//    this.get('/test', function() {
//        console.log('hello');     
//    });
//});
//app.run('/test');